Library to parse cwl files
# Differences with cwl

- each input must have a key ```inputType```, its value must be one of:
  - definition : the value of this input must be defined when the user draws the workflow
  - execution : the value of this input must be defined when the user launches the workflow
  - pipeline : the value of this input comes from a precedent step of the workflow
  - orchestrator : the value of this input will be defined by the pipeline orchestrator (used in data providers)

# TaskList
## Command Line Tool
- [X] CommandLineTool
    - [X] inputs as list
    - [X] inputs as map
    - [X] outputs as list
    - [ ] outputs as map
    - [X] class
    - [X] id
    - [X] label
    - [X] doc
    - [X] requirements as list
    - [X] requirements as map
    - [ ] hints as list
    - [ ] hints as map
    - [ ] cwlVersion
    - [ ] intent
    - [X] baseCommand
    - [X] arguments
    - [ ] stdin
    - [ ] stderr
    - [ ] stdout
    - [X] successCodes
    - [X] temporaryFailCodes
    - [X] permanentFailCodes
    
- [X] CommandInputParameter
    - [X] type
    - [X] label
    - [ ] secondaryFiles
    - [X] streamable
    - [X] doc
    - [X] id
    - [X] format
    - [X] loadContents
    - [ ] loadListing
    - [ ] default
    - [X] inputBinding
    
- [X] CommandLineBinding
    - [ ] loadContents (deprecated)
    - [X] position
    - [X] prefix
    - [X] separate
    - [X] itemSeparator
    - [X] valueFrom
    - [X] shellQuote

- [X] CWLType
    - [ ] null
    - [X] boolean
    - [X] int
    - [X] long
    - [X] float
    - [X] double
    - [X] string
    - [X] File
    - [X] Directory

- [ ] stdin

- [X] CommandInputRecordSchema
    - [X] type
    - [X] fields as list
    - [X] fields as map
    - [X] label
    - [X] doc
    - [X] name
    - [X] inputBinding
    
- [X] CommandInputRecordField
    - [X] name  
    - [X] type  
    - [X] doc  
    - [X] label  
    - [ ] secondaryFiles  
    - [X] streamable  
    - [X] format  
    - [X] loadContents  
    - [ ] loadListing  
    - [X] inputBinding
    
- [X] CommandInputEnumSchema
    - [X] symbols
    - [X] type
    - [X] label
    - [X] doc
    - [X] name
    - [X] inputBinding
    
- [X] CommandInputArraySchema
    - [X] items
    - [X] type
    - [X] label
    - [X] doc
    - [X] name
    - [X] inputBinding
    
- [X] CommandOutputParameter
    - [X] type
    - [X] label
    - [ ] secondaryFiles
    - [X] streamable
    - [X] doc
    - [X] id
    - [X] format
    - [X] outputBinding
    
- [ ] stdout
- [ ] stderr

- [X] CommandOutputRecordSchema
    - [X] type
    - [X] fields
    - [X] label
    - [X] doc
    - [X] name
    
- [X] CommandOutputRecordField
    - [X] name
    - [X] type
    - [X] doc
    - [X] label
    - [ ] secondaryFiles
    - [X] streamable
    - [X] format
    - [X] outputBinding
    
- [X] CommandOutputEnumSchema
    - [X] symbols
    - [X] type
    - [X] label
    - [X] doc
    - [X] name
    
- [X] CommandOutputArraySchema
    - [X] items
    - [X] type
    - [X] label
    - [X] doc
    - [X] name
    
- [X] CommandOutputBinding
    - [X] loadContents
    - [X] loadListing
    - [X] glob
    - [X] outputEval
    
- [ ] InlineJavascriptRequirement
- [ ] SchemaDefRequirement
- [ ] LoadListingRequirement
- [X] DockerRequirement
    - [X] class
    - [X] dockerPull
    - [X] dockerLoad
    - [X] dockerFile
    - [X] dockerImport
    - [X] dockerImageId
    - [X] dockerOutputDirectory
- [ ] SoftwareRequirement
- [ ] SoftwarePackage
- [ ] InitialWorkdirRequirement
- [ ] Dirent
- [ ] EnvVarRequirement
- [ ] EnvironmentDef
- [ ] ShellCommandRequirement
- [ ] ResourceRequirement
- [ ] WorkReuse
- [ ] NetworkAccess
- [ ] InplaceUpdateRequirement
- [ ] ToolTimeLimit
    
# Custom Types

```
DateRange:
{
  start: string representation of time compliant to RFC3339 e.g. 1985-04-12T23:20:50.52Z
  end: string representation of time compliant to RFC3339 e.g. 1985-04-12T23:20:50.52Z
}
```

```
BoundingBox:
{
  TODO
}
```

    

