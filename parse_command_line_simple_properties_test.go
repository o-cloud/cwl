package cwl

import (
	"github.com/stretchr/testify/assert"
	"os"
	"strings"
	"testing"
)

const simpleParametersTestFile = "test/test_simple_parameters.cwl"

func TestParseSimpleProperties(t *testing.T) {
	tool, err := ParseFile(simpleParametersTestFile, t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	assert.Equal(t, "test simple parameters", tool.Label)
	assert.Equal(t, "CommandLineTool", tool.Class)
	assert.Equal(t, "14fe45", tool.ID)
	assert.Equal(t, "input.txt", tool.Stdin)
	assert.Equal(t, "output.txt", tool.Stdout)
	assert.Equal(t, "error.txt", tool.Stderr)
	assert.Equal(t, "A documentation string for this object, or an array of strings which should be concatenated.", tool.Doc)
	assert.Equal(t, []int{0, 1, 2}, tool.SuccessCodes)
	assert.Equal(t, []int{-10, -11}, tool.TemporaryFailCodes)
	assert.Equal(t, []int{-1, -3}, tool.PermanentFailCodes)
	assert.Equal(t, "v1.2", tool.cwlVersion)
	assert.Equal(t, []string{"gdal_translate"}, tool.BaseCommand)
}

func TestParseDocStringArray(t *testing.T) {
	docSlice := []string{"A documentation string", "for this object,", "or an array of strings which should be concatenated."}
	f, err := os.Open(simpleParametersTestFile)
	if err != nil {
		t.Fatalf("Cannot open file %s", simpleParametersTestFile)
	}
	defer f.Close()
	y := ReplaceInYaml("doc", docSlice, f, t)
	tool, err := ParseTool(y)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	assert.Equal(t, strings.Join(docSlice, " "), tool.Doc)
}

func TestParseBaseCommandArray(t *testing.T) {
	commands := []string{"git", "clone"}
	f, err := os.Open(simpleParametersTestFile)
	if err != nil {
		t.Fatalf("Cannot open file %s", simpleParametersTestFile)
	}
	defer f.Close()
	y := ReplaceInYaml("baseCommand", commands, f, t)
	tool, err := ParseTool(y)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	assert.Equal(t, commands, tool.BaseCommand)
}

func TestFailIfNoLabel(t *testing.T) {
	f, err := os.Open(simpleParametersTestFile)
	if err != nil {
		t.Fatalf("Cannot open file %s", simpleParametersTestFile)
	}
	defer f.Close()
	y := DeleteInYaml("label", f, t)
	_, err = ParseTool(y)
	assert.Error(t, err)
}
