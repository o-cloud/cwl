cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
inputs:
  - id: arraySchema
    inputType: definition
    type:
      items: string
      type: array
      label: label
      doc: [doc1,  doc2]
      name: typeName
      inputBinding:
        prefix: "-a"
