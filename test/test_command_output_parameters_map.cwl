cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
outputs:
  outputFormat:
    label: outputFormatLabel
    doc: ["Documentation", "array"]
    outputBinding:
      loadContents: true
      glob: [file1.txt, file2.txt]
      outputEval: ${"aa"}
  fileInput:
    label: fileInputLabel
    doc: "Doc String"
    streamable: true
    format: file_format_1
    outputBinding:
      glob: "*.txt"
