cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
inputs:
  outputFormat:
    label: outputFormatLabel
    doc: ["Documentation", "array"]
    inputType: orchestrator
    inputBinding:
      position: 1
      prefix: -arg
      separate: false
      shellQuote: false
      itemSeparator: " "
  fileInput:
    inputType: pipeline
    label: fileInputLabel
    doc: "Doc String"
    streamable: true
    loadContents: true
    format: file_format_1
