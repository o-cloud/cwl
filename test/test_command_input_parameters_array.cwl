cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
inputs:
  - id: outputFormat
    label: outputFormatLabel
    doc: ["Documentation", "array"]
    inputType: execution
    inputBinding:
      position: 1
      prefix: -arg
      separate: false
      shellQuote: false
      itemSeparator: " "
  - id: fileInput
    inputType: definition
    label: fileInputLabel
    doc: "Doc String"
    streamable: true
    loadContents: true
    format: file_format_1
