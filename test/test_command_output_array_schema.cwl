cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
outputs:
  - id: arraySchema
    type:
      items: string
      type: array
      label: label
      doc: [doc1,  doc2]
      name: typeName
