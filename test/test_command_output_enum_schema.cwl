cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
outputs:
  - id: enumSchema
    type:
      symbols: [a, b ,c]
      type: enum
      label: label
      doc: [doc1,  doc2]
      name: typeName
