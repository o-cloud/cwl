cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
arguments:
  - "-of"
  - position: 1
    prefix: -arg
    separate: false
    shellQuote: false
    itemSeparator: " "
    valueFrom: argValue
  - valueFrom: argValue2
