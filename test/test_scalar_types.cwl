cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
inputs:
  - id: booleanInput
    type: boolean
    inputType: definition
  - id: intInput
    type: int
    inputType: definition
  - id: longInput
    type: long
    inputType: definition
  - id: floatInput
    type: float
    inputType: definition
  - id: doubleInput
    type: double
    inputType: definition
  - id: stringInput
    type: string
    inputType: definition
  - id: fileInput
    type: File
    inputType: definition
  - id: directoryInput
    type: Directory
    inputType: definition
  - id: dateRangeInput
    type: DateRange
    inputType: definition
  - id: boundingBoxInput
    type: BoundingBox
    inputType: definition
outputs:
  - id: booleanOutput
    type: boolean
  - id: intOutput
    type: int
  - id: longOutput
    type: long
  - id: floatOutput
    type: float
  - id: doubleOutput
    type: double
  - id: stringOutput
    type: string
  - id: fileOutput
    type: File
  - id: directoryOutput
    type: Directory
  - id: dateRangeOutput
    type: DateRange
  - id: boundingBoxOutput
    type: BoundingBox
