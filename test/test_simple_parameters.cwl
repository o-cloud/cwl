cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
id: 14fe45
stdin: input.txt
stdout: output.txt
stderr: error.txt
doc: A documentation string for this object, or an array of strings which should be concatenated.
successCodes: [0,1,2]
temporaryFailCodes: [-10, -11]
permanentFailCodes: [-1, -3]
