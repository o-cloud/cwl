cwlVersion: v1.2
class: CommandLineTool
label: test simple parameters
baseCommand: gdal_translate
inputs:
  - id: recordSchema
    inputType: definition
    type:
      type: record
      label: label
      doc: [doc1,  doc2]
      name: typeName
      fields:
        - name: field0
          type: string
          doc: [field0, doc]
          label: field0Label
          inputBinding:
            prefix: "-f0"
        - name: field1
          type: File
          streamable: true
          loadContents: true
          format: format.txt

      inputBinding:
        prefix: "-a"
