package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseOutputsArray(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_parameters_array.cwl", t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	assert.Equal(t, len(tool.Outputs), 2)
	outputFormat, ok := tool.Outputs["outputFormat"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "outputFormatLabel", outputFormat.Label)
	assert.Equal(t, "Documentation array", outputFormat.Doc)
	assert.Equal(t, false, outputFormat.Streamable)
	assert.Equal(t, true, outputFormat.Binding.LoadContents)
	assert.Equal(t, 2, len(outputFormat.Binding.Glob))
	assert.Equal(t, "file1.txt", outputFormat.Binding.Glob[0])
	assert.Equal(t, "file2.txt", outputFormat.Binding.Glob[1])
	assert.Equal(t, "${\"aa\"}", outputFormat.Binding.OutputEval)

	fileInput, ok := tool.Outputs["fileInput"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "fileInputLabel", fileInput.Label)
	assert.Equal(t, "Doc String", fileInput.Doc)
	assert.Equal(t, true, fileInput.Streamable)
	assert.Equal(t, 1, len(fileInput.Format))
	assert.Equal(t, "file_format_1", fileInput.Format[0])
	assert.Equal(t, 1, len(fileInput.Binding.Glob))
	assert.Equal(t, "*.txt", fileInput.Binding.Glob[0])
}

func TestParseOutputsMap(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_parameters_map.cwl", t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	assert.Equal(t, len(tool.Outputs), 2)
	outputFormat, ok := tool.Outputs["outputFormat"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "outputFormatLabel", outputFormat.Label)
	assert.Equal(t, "Documentation array", outputFormat.Doc)
	assert.Equal(t, false, outputFormat.Streamable)
	assert.Equal(t, true, outputFormat.Binding.LoadContents)
	assert.Equal(t, 2, len(outputFormat.Binding.Glob))
	assert.Equal(t, "file1.txt", outputFormat.Binding.Glob[0])
	assert.Equal(t, "file2.txt", outputFormat.Binding.Glob[1])
	assert.Equal(t, "${\"aa\"}", outputFormat.Binding.OutputEval)

	fileInput, ok := tool.Outputs["fileInput"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "fileInputLabel", fileInput.Label)
	assert.Equal(t, "Doc String", fileInput.Doc)
	assert.Equal(t, true, fileInput.Streamable)
	assert.Equal(t, 1, len(fileInput.Format))
	assert.Equal(t, "file_format_1", fileInput.Format[0])
	assert.Equal(t, 1, len(fileInput.Binding.Glob))
	assert.Equal(t, "*.txt", fileInput.Binding.Glob[0])
}
