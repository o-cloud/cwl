package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CommandInputUnionSchema struct {
	Types []CommandInputTypeWrapper `json:"types"`
	Type  string                    `json:"type"`
}

type CommandOutputUnionSchema struct {
	Types []CommandOutputTypeWrapper `json:"types"`
	Type  string                     `json:"type"`
}

func (c CommandInputUnionSchema) isCommandInputType() {}

func (c CommandOutputUnionSchema) isCommandOutputType() {}

func parseCommandInputUnionSchema(node yaml.Node) (CommandInputUnionSchema, error) {
	switch node.(type) {
	case yaml.List:
		union := new(CommandInputUnionSchema)
		union.Type = "union"
		for i, t := range node.(yaml.List) {
			inputType, err := parseInputType(t)
			if err != nil {
				return *union, fmt.Errorf("cannot parse union input type, cannot parse type at index %d, %s", i, err)
			}
			union.Types = append(union.Types, CommandInputTypeWrapper{Wrapped: inputType})
		}
		return *union, nil
	default:
		return CommandInputUnionSchema{}, fmt.Errorf("cannot parse union input type, expected list found %T", node)
	}
}

func parseCommandOutputUnionSchema(node yaml.Node) (CommandOutputUnionSchema, error) {
	switch node.(type) {
	case yaml.List:
		union := new(CommandOutputUnionSchema)
		union.Type = "union"
		for i, t := range node.(yaml.List) {
			outputType, err := parseOutputType(t)
			if err != nil {
				return *union, fmt.Errorf("cannot parse union output type, cannot parse type at index %d, %s", i, err)
			}
			union.Types = append(union.Types, CommandOutputTypeWrapper{Wrapped: outputType})
		}
		return *union, nil
	default:
		return CommandOutputUnionSchema{}, fmt.Errorf("cannot parse union input type, expected list found %T", node)
	}
}
