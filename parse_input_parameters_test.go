package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const commandInputParametersArrayTestFile = "test/test_command_input_parameters_array.cwl"

func TestParseInputsArray(t *testing.T) {
	tool, err := ParseFile(commandInputParametersArrayTestFile, t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	assert.Equal(t, len(tool.Inputs), 2)
	outputFormat, ok := tool.Inputs["outputFormat"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "outputFormatLabel", outputFormat.Label)
	assert.Equal(t, "Documentation array", outputFormat.Doc)
	assert.Equal(t, false, outputFormat.Streamable)
	assert.Equal(t, false, outputFormat.LoadContents)
	assert.Equal(t, 1, outputFormat.InputBinding.Position)
	assert.Equal(t, "-arg", outputFormat.InputBinding.Prefix)
	assert.Equal(t, false, outputFormat.InputBinding.Separate)
	assert.Equal(t, false, outputFormat.InputBinding.ShellQuote)
	assert.Equal(t, " ", outputFormat.InputBinding.ItemSeparator)
	assert.Equal(t, "", outputFormat.InputBinding.ValueFrom)
	assert.Equal(t, "execution", outputFormat.InputType)

	fileInput, ok := tool.Inputs["fileInput"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "fileInputLabel", fileInput.Label)
	assert.Equal(t, "Doc String", fileInput.Doc)
	assert.Equal(t, true, fileInput.Streamable)
	assert.Equal(t, true, fileInput.LoadContents)
	assert.Equal(t, 1, len(fileInput.Format))
	assert.Equal(t, "file_format_1", fileInput.Format[0])
	assert.Equal(t, "definition", fileInput.InputType)
}

func TestParseInputsMap(t *testing.T) {
	tool, err := ParseFile("test/test_command_input_parameters_map.cwl", t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	assert.Equal(t, len(tool.Inputs), 2)
	outputFormat, ok := tool.Inputs["outputFormat"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "outputFormatLabel", outputFormat.Label)
	assert.Equal(t, "Documentation array", outputFormat.Doc)
	assert.Equal(t, false, outputFormat.Streamable)
	assert.Equal(t, false, outputFormat.LoadContents)
	assert.Equal(t, 1, outputFormat.InputBinding.Position)
	assert.Equal(t, "-arg", outputFormat.InputBinding.Prefix)
	assert.Equal(t, false, outputFormat.InputBinding.Separate)
	assert.Equal(t, false, outputFormat.InputBinding.ShellQuote)
	assert.Equal(t, " ", outputFormat.InputBinding.ItemSeparator)
	assert.Equal(t, "", outputFormat.InputBinding.ValueFrom)
	assert.Equal(t, "orchestrator", outputFormat.InputType)

	fileInput, ok := tool.Inputs["fileInput"]
	assert.Equal(t, true, ok)
	assert.Equal(t, "fileInputLabel", fileInput.Label)
	assert.Equal(t, "Doc String", fileInput.Doc)
	assert.Equal(t, true, fileInput.Streamable)
	assert.Equal(t, true, fileInput.LoadContents)
	assert.Equal(t, 1, len(fileInput.Format))
	assert.Equal(t, "file_format_1", fileInput.Format[0])
	assert.Equal(t, "pipeline", fileInput.InputType)
}
