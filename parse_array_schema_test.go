package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCommandInputArraySchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_input_array_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Inputs["arraySchema"].Type.Wrapped
	assert.IsType(t, CommandInputArraySchema{}, ty)
	array := ty.(CommandInputArraySchema)
	assert.Equal(t, "array", array.Type)
	assert.IsType(t, StringType{}, array.Items.Wrapped)
	assert.Equal(t, "label", array.Label)
	assert.Equal(t, "doc1 doc2", array.Doc)
	assert.Equal(t, "typeName", array.Name)
	assert.Equal(t, "-a", array.InputBinding.Prefix)
}

func TestParseCommandOutputArraySchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_array_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Outputs["arraySchema"].Type.Wrapped
	assert.IsType(t, CommandOutputArraySchema{}, ty)
	array := ty.(CommandOutputArraySchema)
	assert.Equal(t, "array", array.Type)
	assert.IsType(t, StringType{}, array.Items.Wrapped)
	assert.Equal(t, "label", array.Label)
	assert.Equal(t, "doc1 doc2", array.Doc)
	assert.Equal(t, "typeName", array.Name)
}
