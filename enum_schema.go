package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type commonEnumSchema struct {
	Symbols []string `json:"symbols"`
	Label   string   `json:"label"`
	Doc     string   `json:"doc"`
	Name    string   `json:"name"`
	Type    string   `json:"type"`
}

type CommandInputEnumSchema struct {
	InputBinding CommandLineBinding `json:"inputBinding"`
	commonEnumSchema
}

type CommandOutputEnumSchema struct {
	commonEnumSchema
}

func (c CommandInputEnumSchema) isCommandInputType() {}

func (c CommandOutputEnumSchema) isCommandOutputType() {}

func parseCommandInputEnumSchema(node yaml.Node) (CommandInputEnumSchema, error) {
	switch node.(type) {
	case yaml.Map:
		enum := new(CommandInputEnumSchema)
		common, err := parseCommonEnumSchema(node.(yaml.Map))
		if err != nil {
			return *enum, err
		}
		enum.Name = common.Name
		enum.Label = common.Label
		enum.Doc = common.Doc
		enum.Symbols = common.Symbols
		enum.Type = common.Type
		for k, v := range node.(yaml.Map) {
			switch k {
			case "inputBinding":
				binding, err := parseCommandLineBinding(v)
				if err != nil {
					return *enum, fmt.Errorf("cannot parse enum input type, %s", err)
				}
				enum.InputBinding = *binding
			}
		}
		return *enum, nil
	default:
		return CommandInputEnumSchema{}, fmt.Errorf("cannot parse enum input type, expected map found %T", node)
	}
}

func parseCommandOutputEnumSchema(node yaml.Node) (CommandOutputEnumSchema, error) {
	switch node.(type) {
	case yaml.Map:
		enum := new(CommandOutputEnumSchema)
		common, err := parseCommonEnumSchema(node.(yaml.Map))
		if err != nil {
			return *enum, err
		}
		enum.Name = common.Name
		enum.Label = common.Label
		enum.Doc = common.Doc
		enum.Symbols = common.Symbols
		enum.Type = common.Type
		return *enum, nil
	default:
		return CommandOutputEnumSchema{}, fmt.Errorf("cannot parse enum output type, expect Map found %T", node)
	}
}

func parseCommonEnumSchema(node yaml.Map) (commonEnumSchema, error) {
	common := new(commonEnumSchema)
	common.Type = "enum"
	for k, v := range node {
		switch k {
		case "symbols":
			symbols, err := v.StringList()
			if err != nil {
				return *common, fmt.Errorf("cannot parse enum type, cannot parse symbols, %s", err)
			}
			common.Symbols = symbols
		case "label":
			s, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse enum type, cannot parse label, %s", err)
			}
			common.Label = s
		case "doc":
			doc, err := parseDoc(v)
			if err != nil {
				return *common, fmt.Errorf("cannot parse enum type, %s", err)
			}
			common.Doc = doc
		case "name":
			s, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse enum type, cannot parse name, %s", err)
			}
			common.Name = s
		}
	}
	return *common, nil
}
