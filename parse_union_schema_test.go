package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCommandInputUnionSchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_input_union_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Inputs["unionSchema"].Type.Wrapped
	assert.IsType(t, CommandInputUnionSchema{}, ty)

	union := ty.(CommandInputUnionSchema)
	assert.Equal(t, 3, len(union.Types))
	assert.Equal(t, "union", union.Type)
	assert.IsType(t, CommandInputArraySchema{}, union.Types[0].Wrapped)
	assert.IsType(t, CommandInputEnumSchema{}, union.Types[1].Wrapped)
	assert.IsType(t, IntegerType{}, union.Types[2].Wrapped)

	array := union.Types[0].Wrapped.(CommandInputArraySchema)
	assert.IsType(t, StringType{}, array.Items.Wrapped)
	assert.Equal(t, "label", array.Label)
	assert.Equal(t, "doc1 doc2", array.Doc)
	assert.Equal(t, "typeName", array.Name)
	assert.Equal(t, "-a", array.InputBinding.Prefix)

	enum := union.Types[1].Wrapped.(CommandInputEnumSchema)
	assert.Equal(t, 3, len(enum.Symbols))
	assert.Equal(t, "a", enum.Symbols[0])
	assert.Equal(t, "b", enum.Symbols[1])
	assert.Equal(t, "c", enum.Symbols[2])
	assert.Equal(t, "label", enum.Label)
	assert.Equal(t, "doc1 doc2", enum.Doc)
	assert.Equal(t, "typeName", enum.Name)
	assert.Equal(t, "-a", enum.InputBinding.Prefix)
}

func TestParseCommandOutputUnionSchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_union_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Outputs["unionSchema"].Type.Wrapped
	assert.IsType(t, CommandOutputUnionSchema{}, ty)

	union := ty.(CommandOutputUnionSchema)
	assert.Equal(t, 3, len(union.Types))
	assert.Equal(t, "union", union.Type)
	assert.IsType(t, CommandOutputArraySchema{}, union.Types[0].Wrapped)
	assert.IsType(t, CommandOutputEnumSchema{}, union.Types[1].Wrapped)
	assert.IsType(t, IntegerType{}, union.Types[2].Wrapped)

	array := union.Types[0].Wrapped.(CommandOutputArraySchema)
	assert.IsType(t, StringType{}, array.Items.Wrapped)
	assert.Equal(t, "label", array.Label)
	assert.Equal(t, "doc1 doc2", array.Doc)
	assert.Equal(t, "typeName", array.Name)

	enum := union.Types[1].Wrapped.(CommandOutputEnumSchema)
	assert.Equal(t, 3, len(enum.Symbols))
	assert.Equal(t, "a", enum.Symbols[0])
	assert.Equal(t, "b", enum.Symbols[1])
	assert.Equal(t, "c", enum.Symbols[2])
	assert.Equal(t, "label", enum.Label)
	assert.Equal(t, "doc1 doc2", enum.Doc)
	assert.Equal(t, "typeName", enum.Name)
}
