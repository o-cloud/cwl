package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCommandInputEnumSchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_input_enum_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Inputs["enumSchema"].Type.Wrapped
	assert.IsType(t, CommandInputEnumSchema{}, ty)
	enum := ty.(CommandInputEnumSchema)
	assert.Equal(t, "enum", enum.Type)
	assert.Equal(t, 3, len(enum.Symbols))
	assert.Equal(t, "a", enum.Symbols[0])
	assert.Equal(t, "b", enum.Symbols[1])
	assert.Equal(t, "c", enum.Symbols[2])
	assert.Equal(t, "label", enum.Label)
	assert.Equal(t, "doc1 doc2", enum.Doc)
	assert.Equal(t, "typeName", enum.Name)
	assert.Equal(t, "-a", enum.InputBinding.Prefix)
}

func TestParseCommandOutputEnumSchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_enum_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Outputs["enumSchema"].Type.Wrapped
	assert.IsType(t, CommandOutputEnumSchema{}, ty)
	enum := ty.(CommandOutputEnumSchema)
	assert.Equal(t, "enum", enum.Type)
	assert.Equal(t, 3, len(enum.Symbols))
	assert.Equal(t, "a", enum.Symbols[0])
	assert.Equal(t, "b", enum.Symbols[1])
	assert.Equal(t, "c", enum.Symbols[2])
	assert.Equal(t, "label", enum.Label)
	assert.Equal(t, "doc1 doc2", enum.Doc)
	assert.Equal(t, "typeName", enum.Name)
}
