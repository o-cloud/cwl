package cwl

import (
	"bytes"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"os"
	"testing"
)

func ParseFile(path string, t *testing.T) (*CommandLineTool, error) {
	f, err := os.Open(path)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	defer f.Close()

	return ParseTool(f)
}

func ReplaceInYaml(path string, value interface{}, reader io.Reader, t *testing.T) io.Reader {
	var o map[string]interface{}
	bytesIn, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Fatalf("cannot read yaml")
		return nil
	}
	if err = yaml.Unmarshal(bytesIn, &o); err != nil {
		t.Fatalf("cannot read yaml")
		return nil
	}
	o[path] = value
	result, err := yaml.Marshal(&o)
	if err != nil {
		t.Fatalf("cannot write yaml")
		return nil
	}
	return bytes.NewReader(result)
}

func DeleteInYaml(path string, reader io.Reader, t *testing.T) io.Reader {
	var o map[string]interface{}
	bytesIn, err := ioutil.ReadAll(reader)
	if err != nil {
		t.Fatalf("cannot read yaml")
		return nil
	}
	if err = yaml.Unmarshal(bytesIn, &o); err != nil {
		t.Fatalf("cannot read yaml")
		return nil
	}
	delete(o, path)
	result, err := yaml.Marshal(&o)
	if err != nil {
		t.Fatalf("cannot write yaml")
		return nil
	}
	return bytes.NewReader(result)
}
