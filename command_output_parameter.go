package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CommandOutputParameter struct {
	Label      string                   `json:"label"`
	Streamable bool                     `json:"streamable"`
	Doc        string                   `json:"doc"`
	Format     []string                 `json:"format"`
	Binding    CommandOutputBinding     `json:"binding"`
	Type       CommandOutputTypeWrapper `json:"type"`
}

type Outputs map[string]CommandOutputParameter

type CommandOutputType interface {
	isCommandOutputType()
}

func parseCommandOutputParameter(node yaml.Node) (CommandOutputParameter, error) {
	switch node.(type) {
	case yaml.Map:
		p := new(CommandOutputParameter)
		for key, value := range node.(yaml.Map) {
			switch key {
			case "label":
				str, err := value.String()
				if err != nil {
					return *p, fmt.Errorf("cannot parse label, %s", err)
				}
				p.Label = str
			case "streamable":
				b, err := value.Boolean()
				if err != nil {
					return *p, fmt.Errorf("cannot parse streamable, %s", err)
				}
				p.Streamable = b
			case "doc":
				doc, err := parseDoc(value)
				if err != nil {
					return *p, err
				}
				p.Doc = doc
			case "format":
				list, err := value.StringList()
				if err != nil {
					return *p, fmt.Errorf("cannot parse format, %s", err)
				}
				p.Format = list
			case "outputBinding":
				binding, err := parseCommandOutputBinding(value)
				if err != nil {
					return *p, err
				}
				p.Binding = binding
			case "type":
				t, err := parseOutputType(value)
				if err != nil {
					return *p, err
				}
				p.Type = CommandOutputTypeWrapper{Wrapped: t}
			}
		}
		return *p, nil
	default:
		return CommandOutputParameter{}, fmt.Errorf("cannot parse output, expected type Map, found %T", node)
	}
}

func parseOutputs(node yaml.Node) (Outputs, error) {
	switch node.(type) {
	case yaml.List:
		outputs := make(Outputs, len(node.(yaml.List)))
		for i, value := range node.(yaml.List) {
			out, ok := value.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse output[%d], expected type Map, found %T", i, value)
			}
			id, ok := out["id"]
			if !ok {
				return nil, fmt.Errorf("cannot parse output[%d], key id not found", i)
			}
			idStr, err := id.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse output[%d], cannot parse id, %s", i, err)
			}
			outputParameter, err := parseCommandOutputParameter(out)
			if err != nil {
				return nil, fmt.Errorf("cannot parse output %s, %s", idStr, err)
			}
			outputs[idStr] = outputParameter
		}
		return outputs, nil
	case yaml.Map:
		outputs := make(Outputs, len(node.(yaml.Map)))
		for key, value := range node.(yaml.Map) {
			outputParameter, err := parseCommandOutputParameter(value)
			if err != nil {
				return nil, fmt.Errorf("cannot parse output %s, %s", key, err)
			}
			outputs[key] = outputParameter
		}
		return outputs, nil
	default:
		return nil, fmt.Errorf("cannot parse outputs, expected List or Map found %T", node)
	}
}

func parseOutputType(node yaml.Node) (CommandOutputType, error) {
	switch node.(type) {
	case yaml.Scalar:
		return parseCwlType(node.(yaml.Scalar))
	case yaml.Map:
		typeScalar, ok := node.(yaml.Map)["type"]
		if !ok {
			return nil, fmt.Errorf("cannot parse type, map doesn't contains a type key")
		}
		typeStr, err := typeScalar.String()
		if !ok {
			return nil, fmt.Errorf("cannot parse type, error while reading type key, %s", err)
		}
		switch typeStr {
		case "array":
			return parseCommandOutputArraySchema(node)
		case "enum":
			return parseCommandOutputEnumSchema(node)
		case "record":
			return parseCommandOutputRecordSchema(node)
		default:
			return nil, fmt.Errorf("cannot parse type, unknown type %s", typeStr)
		}
	case yaml.List:
		return parseCommandOutputUnionSchema(node)
	default:
		return nil, fmt.Errorf("cannot parse type, expected string, map or list, found %T", node)
	}
}
