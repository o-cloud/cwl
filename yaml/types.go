package yaml

import "fmt"

type Node interface {
	isNode()
	String() (string, error)
	StringList() ([]string, error)
	Integer() (int, error)
	Float() (float32, error)
	Boolean() (bool, error)
}

type Map map[string]Node

func (m Map) isNode() {
}

func (m Map) String() (string, error) {
	return "", fmt.Errorf("expected string but found map")
}

func (m Map) Integer() (int, error) {
	return 0, fmt.Errorf("expected integer but found map")
}

func (m Map) Float() (float32, error) {
	return 0, fmt.Errorf("expected float but found map")
}

func (m Map) Boolean() (bool, error) {
	return false, fmt.Errorf("expected boolean but found map")
}

func (m Map) StringList() ([]string, error) {
	return nil, fmt.Errorf("expected string or list of string but found map")
}

type List []Node

func (l List) isNode() {
}

func (l List) String() (string, error) {
	return "", fmt.Errorf("expected string but found list")
}

func (l List) Integer() (int, error) {
	return 0, fmt.Errorf("expected int but found list")
}

func (l List) Float() (float32, error) {
	return 0, fmt.Errorf("expected float but found list")
}

func (l List) Boolean() (bool, error) {
	return false, fmt.Errorf("expected boolean but found list")
}

func (l List) IntList() ([]int, error) {
	result := make([]int, len(l))
	for i, v := range l {
		number, err := v.Integer()
		if err != nil {
			return nil, fmt.Errorf("cannot parse int array, %s", err)
		}
		result[i] = number
	}
	return result, nil
}

func (l List) StringList() ([]string, error) {
	result := make([]string, len(l))
	for i, v := range l {
		str, err := v.String()
		if err != nil {
			return nil, fmt.Errorf("cannot parse string array, %s", err)
		}
		result[i] = str
	}
	return result, nil
}

type Scalar struct {
	value interface{}
}

func (s Scalar) New(value interface{}) *Scalar {
	return &Scalar{value: value}
}

func (s Scalar) isNode() {
}

func (s Scalar) String() (string, error) {
	str, ok := s.value.(string)
	if !ok {
		return "", fmt.Errorf("expected string but found %T", s.value)
	}
	return str, nil
}

func (s Scalar) Integer() (int, error) {
	integer, ok := s.value.(int)
	if !ok {
		return 0, fmt.Errorf("expected integer but found %T", s.value)
	}
	return integer, nil
}

func (s Scalar) Float() (float32, error) {
	f, ok := s.value.(float32)
	if !ok {
		return 0, fmt.Errorf("expected float but found %T", s.value)
	}
	return f, nil
}

func (s Scalar) Boolean() (bool, error) {
	b, ok := s.value.(bool)
	if !ok {
		return false, fmt.Errorf("expected bool but found %T", s.value)
	}
	return b, nil
}

func (s Scalar) StringList() ([]string, error) {
	str, err := s.String()
	if err != nil {
		return nil, fmt.Errorf("expected string or list of string but found %T", s.value)
	}
	return []string{str}, nil
}
