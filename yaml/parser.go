package yaml

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
)

func Parse(reader io.Reader) (Map, error) {
	bytes, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}
	var object map[string]interface{}
	err = yaml.Unmarshal(bytes, &object)
	if err != nil {
		return nil, err
	}
	node, err := parseStringMap(object)
	if err != nil {
		return nil, err
	}
	return node, err
}

func parseStringMap(o map[string]interface{}) (Map, error) {
	m := make(Map, len(o))
	for key, val := range o {
		node, err := parseNode(val)
		if err != nil {
			return nil, err
		}
		m[key] = node
	}
	return m, nil
}

func parseNode(o interface{}) (Node, error) {
	switch o.(type) {
	case map[string]interface{}:
		return parseStringMap(o.(map[string]interface{}))
	case map[interface{}]interface{}:
		return parseInterfaceMap(o.(map[interface{}]interface{}))
	case []interface{}:
		return parseList(o.([]interface{}))
	default:
		return Scalar{value: o}, nil
	}
}

func parseList(o []interface{}) (List, error) {
	l := make(List, len(o))
	for i, val := range o {
		node, err := parseNode(val)
		if err != nil {
			return nil, err
		}
		l[i] = node
	}
	return l, nil
}

func parseInterfaceMap(o map[interface{}]interface{}) (Map, error) {
	m := make(map[string]interface{}, len(o))
	for key, val := range o {
		strKey, ok := key.(string)
		if !ok {
			return nil, fmt.Errorf("cannot parse key %v, expected type string found %T", key, key)
		}
		m[strKey] = val
	}
	return parseStringMap(m)
}
