package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const argumentsTestFile = "test/test_parse_arguments.cwl"

func TestParseStringArgument(t *testing.T) {
	tool, err := ParseFile(argumentsTestFile, t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	assert.Equal(t, len(tool.Arguments), 3)
	arg := tool.Arguments[0]
	assert.Equal(t, arg.Position, 0)
	assert.Equal(t, arg.Prefix, "")
	assert.Equal(t, arg.Separate, true)
	assert.Equal(t, arg.ItemSeparator, "")
	assert.Equal(t, arg.ValueFrom, "-of")
	assert.Equal(t, arg.ShellQuote, true)
}

func TestParseCommandLineBindingArgument(t *testing.T) {
	tool, err := ParseFile(argumentsTestFile, t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	assert.Equal(t, len(tool.Arguments), 3)
	arg := tool.Arguments[1]
	assert.Equal(t, arg.Position, 1)
	assert.Equal(t, arg.Prefix, "-arg")
	assert.Equal(t, arg.Separate, false)
	assert.Equal(t, arg.ItemSeparator, " ")
	assert.Equal(t, arg.ValueFrom, "argValue")
	assert.Equal(t, arg.ShellQuote, false)
}

func TestParseCommandLineBindingArgumentWithDefaultValues(t *testing.T) {
	tool, err := ParseFile(argumentsTestFile, t)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	assert.Equal(t, len(tool.Arguments), 3)
	arg := tool.Arguments[2]
	assert.Equal(t, arg.Position, 0)
	assert.Equal(t, arg.Prefix, "")
	assert.Equal(t, arg.Separate, true)
	assert.Equal(t, arg.ItemSeparator, "")
	assert.Equal(t, arg.ValueFrom, "argValue2")
	assert.Equal(t, arg.ShellQuote, true)
}
