package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
	"io"
	"strings"
)

type CommandLineTool struct {
	Label              string               `json:"label"`
	Class              string               `json:"class"`
	ID                 string               `json:"id"`
	Stdin              string               `json:"stdin"`
	Stdout             string               `json:"stdout"`
	Stderr             string               `json:"stderr"`
	Doc                string               `json:"doc"`
	SuccessCodes       []int                `json:"successCodes"`
	TemporaryFailCodes []int                `json:"temporaryFailCodes"`
	PermanentFailCodes []int                `json:"permanentFailCodes"`
	cwlVersion         string               `json:"cwlVersion"`
	BaseCommand        []string             `json:"baseCommand"`
	Arguments          []CommandLineBinding `json:"arguments"`
	Inputs             Inputs               `json:"inputs"`
	Outputs            Outputs              `json:"outputs"`
	Requirements       Requirements         `json:"requirements"`
}

func ParseTool(reader io.Reader) (*CommandLineTool, error) {
	yamlMap, err := yaml.Parse(reader)
	if err != nil {
		return nil, err
	}
	tool := new(CommandLineTool)
	tool.Class = "CommandLineTool"
	_, ok := yamlMap["label"]
	if !ok {
		return nil, fmt.Errorf("command line tool must have a label")
	}
	for key, val := range yamlMap {
		switch key {
		case "label":
			s, err := val.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse %s, %s", key, err)
			}
			tool.Label = s
		case "id":
			s, err := val.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse %s, %s", key, err)
			}
			tool.ID = s
		case "stdin":
			s, err := val.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse %s, %s", key, err)
			}
			tool.Stdin = s
		case "stdout":
			s, err := val.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse %s, %s", key, err)
			}
			tool.Stdout = s
		case "stderr":
			s, err := val.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse %s, %s", key, err)
			}
			tool.Stderr = s
		case "cwlVersion":
			s, err := val.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse %s, %s", key, err)
			}
			tool.cwlVersion = s
		case "doc":
			str, err := parseDoc(val)
			if err != nil {
				return nil, err
			}
			tool.Doc = str
		case "successCodes":
			codes, err := parseIntArray(val)
			if err != nil {
				return nil, fmt.Errorf("error parsing success codes, %s", err)
			}
			tool.SuccessCodes = codes
		case "temporaryFailCodes":
			codes, err := parseIntArray(val)
			if err != nil {
				return nil, fmt.Errorf("error parsing temporary fail codes, %s", err)
			}
			tool.TemporaryFailCodes = codes
		case "permanentFailCodes":
			codes, err := parseIntArray(val)
			if err != nil {
				return nil, fmt.Errorf("error parsing permanent fail codes, %s", err)
			}
			tool.PermanentFailCodes = codes
		case "baseCommand":
			array, err := val.StringList()
			if err != nil {
				return nil, fmt.Errorf("error parsing base command, %s", err)
			}
			tool.BaseCommand = array
		case "arguments":
			args, err := parseArguments(val)
			if err != nil {
				return nil, err
			}
			tool.Arguments = args
		case "inputs":
			in, err := parseInputs(val)
			if err != nil {
				return nil, err
			}
			tool.Inputs = *in
		case "outputs":
			outputs, err := parseOutputs(val)
			if err != nil {
				return nil, err
			}
			tool.Outputs = outputs
		case "requirements":
			requirements, err := parseRequirements(val)
			if err != nil {
				return nil, err
			}
			tool.Requirements = requirements
		}
	}
	return tool, nil
}

func parseDoc(node yaml.Node) (string, error) {
	list, err := node.StringList()
	if err != nil {
		return "", fmt.Errorf("type of field doc is not valid")
	}
	return strings.Join(list, " "), nil
}

func parseIntArray(value yaml.Node) ([]int, error) {
	switch value.(type) {
	case yaml.List:
		return value.(yaml.List).IntList()
	default:
		return nil, fmt.Errorf("expected int array found %T", value)
	}
}

func parseArguments(node yaml.Node) ([]CommandLineBinding, error) {
	switch node.(type) {
	case yaml.List:
		array := node.(yaml.List)
		//Map scalars to map with valueFrom
		for i, arg := range array {
			switch arg.(type) {
			case yaml.Scalar:
				str, err := arg.String()
				if err != nil {
					return nil, fmt.Errorf("type of arguments[%d] is not valid, %s", i, err)
				}
				m := make(yaml.Map, 1)
				m["valueFrom"] = yaml.Scalar{}.New(str)
				array[i] = m
			}
		}
		result := make([]CommandLineBinding, len(array))
		for i, arg := range array {
			switch arg.(type) {
			case yaml.Map:
				binding, err := parseCommandLineBinding(arg.(yaml.Map))
				if err != nil {
					return nil, fmt.Errorf("cannot parse arguments[%d], %s", i, err)
				}
				result[i] = *binding
			default:
				return nil, fmt.Errorf("type of arguments[%d] is not valid, expected string or map, found %T", i, arg)
			}
		}
		return result, nil
	default:
		return nil, fmt.Errorf("type of field arguments is not valid, expected array found %T", node)
	}
}
