package cwl

import (
	"encoding/json"
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type Requirements []Requirement

func (r *Requirements) UnmarshalJSON(bytes []byte) error {
	if len(bytes) == 0 {
		return nil
	}
	var raw []json.RawMessage
	if err := json.Unmarshal(bytes, &raw); err != nil {
		return err
	}
	for _, elem := range raw {
		var rawElem map[string]interface{}
		if err := json.Unmarshal(elem, &rawElem); err != nil {
			return err
		}
		t, ok := rawElem["type"]
		if !ok {
			return fmt.Errorf("cannot parse requirement, there is no field type in %+v", rawElem)
		}
		str, ok := t.(string)
		if !ok {
			return fmt.Errorf("cannot parse requirement, expected field type to be a string not a %T", t)
		}
		switch str {
		case "DockerRequirement":
			var requirement DockerRequirement
			if err := json.Unmarshal(elem, &requirement); err != nil {
				return err
			}
			*r = append(*r, requirement)
		}
	}
	return nil
}

type Requirement interface {
	isRequirement()
}

type DockerRequirement struct {
	Type                  string `json:"type"`
	DockerPull            string `json:"dockerPull"`
	DockerLoad            string `json:"dockerLoad"`
	DockerFile            string `json:"dockerFile"`
	DockerImport          string `json:"dockerImport"`
	DockerImageId         string `json:"dockerImageId"`
	DockerOutputDirectory string `json:"dockerOutputDirectory"`
}

func (d DockerRequirement) isRequirement() {}

func parseRequirements(node yaml.Node) ([]Requirement, error) {
	switch node.(type) {
	case yaml.List:
		requirements := make([]Requirement, len(node.(yaml.List)))
		for i, v := range node.(yaml.List) {
			requirement, ok := v.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse requirements, cannot parse requirement[%d], expected type Map, found %T", i, v)
			}
			classNode, ok := requirement["class"]
			if !ok {
				return nil, fmt.Errorf("cannot parse requirements, cannot parse requirement[%d], cannot find requirement class", i)
			}
			classStr, err := classNode.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse requirements, cannot parse requirement[%d], cannot parse class, %s", i, err)
			}
			r, err := parseRequirement(classStr, requirement)
			if err != nil {
				return nil, fmt.Errorf("cannot parse requirement, cannot parse requirement[%d], %s", i, err)
			}
			requirements[i] = r
		}
		return requirements, nil
	case yaml.Map:
		requirements := make([]Requirement, len(node.(yaml.Map)))
		i := 0
		for k, v := range node.(yaml.Map) {
			requirement, ok := v.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse requirements, cannot parse requirement %s, expected type Map, found %T", k, v)
			}
			r, err := parseRequirement(k, requirement)
			if err != nil {
				return nil, fmt.Errorf("cannot parse requirement, cannot parse requirement %s, %s", k, err)
			}
			requirements[i] = r
			i++
		}
		return requirements, nil
	default:
		return nil, fmt.Errorf("cannot parse requirements, expected type Map or List, found %T", node)
	}
}

func parseRequirement(class string, node yaml.Map) (Requirement, error) {
	switch class {
	case "DockerRequirement":
		return parseDockerRequirement(node)
	default:
		return nil, fmt.Errorf("cannot parse requirement, unknown class %s", class)
	}
}

func parseDockerRequirement(node yaml.Map) (DockerRequirement, error) {
	r := DockerRequirement{Type: "DockerRequirement"}
	for k, v := range node {
		switch k {
		case "dockerPull":
			s, err := v.String()
			if err != nil {
				return r, fmt.Errorf("cannot parse %s, %s", k, err)
			}
			r.DockerPull = s
		case "dockerLoad":
			s, err := v.String()
			if err != nil {
				return r, fmt.Errorf("cannot parse %s, %s", k, err)
			}
			r.DockerLoad = s
		case "dockerFile":
			s, err := v.String()
			if err != nil {
				return r, fmt.Errorf("cannot parse %s, %s", k, err)
			}
			r.DockerFile = s
		case "dockerImport":
			s, err := v.String()
			if err != nil {
				return r, fmt.Errorf("cannot parse %s, %s", k, err)
			}
			r.DockerImport = s
		case "dockerImageId":
			s, err := v.String()
			if err != nil {
				return r, fmt.Errorf("cannot parse %s, %s", k, err)
			}
			r.DockerImageId = s
		case "dockerOutputDirectory":
			s, err := v.String()
			if err != nil {
				return r, fmt.Errorf("cannot parse %s, %s", k, err)
			}
			r.DockerOutputDirectory = s
		}
	}
	return r, nil
}
