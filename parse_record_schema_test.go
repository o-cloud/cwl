package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseCommandInputRecordSchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_input_record_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Inputs["recordSchema"].Type.Wrapped
	assert.IsType(t, CommandInputRecordSchema{}, ty)
	record := ty.(CommandInputRecordSchema)
	assert.Equal(t, "record", record.Type)
	assert.Equal(t, "label", record.Label)
	assert.Equal(t, "doc1 doc2", record.Doc)
	assert.Equal(t, "typeName", record.Name)
	assert.Equal(t, "-a", record.InputBinding.Prefix)
	assert.Equal(t, 2, len(record.Fields))

	field0 := record.Fields["field0"]
	assert.IsType(t, StringType{}, field0.Type.Wrapped)
	assert.Equal(t, "field0 doc", field0.Doc)
	assert.Equal(t, "field0Label", field0.Label)
	assert.Equal(t, "-f0", field0.InputBinding.Prefix)
	assert.Equal(t, false, field0.Streamable)
	assert.Equal(t, false, field0.LoadContents)

	field1 := record.Fields["field1"]
	assert.IsType(t, FileType{}, field1.Type.Wrapped)
	assert.Equal(t, true, field1.Streamable)
	assert.Equal(t, true, field1.LoadContents)
	assert.Equal(t, 1, len(field1.Format))
	assert.Equal(t, "format.txt", field1.Format[0])
}

func TestParseCommandInputRecordSchemaFieldsMap(t *testing.T) {
	tool, err := ParseFile("test/test_command_input_record_schema_map.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Inputs["recordSchema"].Type.Wrapped
	assert.IsType(t, CommandInputRecordSchema{}, ty)
	record := ty.(CommandInputRecordSchema)
	assert.Equal(t, "record", record.Type)
	assert.Equal(t, "label", record.Label)
	assert.Equal(t, "doc1 doc2", record.Doc)
	assert.Equal(t, "typeName", record.Name)
	assert.Equal(t, "-a", record.InputBinding.Prefix)
	assert.Equal(t, 2, len(record.Fields))

	field0 := record.Fields["field0"]
	assert.IsType(t, StringType{}, field0.Type.Wrapped)
	assert.Equal(t, "field0 doc", field0.Doc)
	assert.Equal(t, "field0Label", field0.Label)
	assert.Equal(t, "-f0", field0.InputBinding.Prefix)
	assert.Equal(t, false, field0.Streamable)
	assert.Equal(t, false, field0.LoadContents)
	field1 := record.Fields["field1"]
	assert.IsType(t, FileType{}, field1.Type.Wrapped)
	assert.Equal(t, true, field1.Streamable)
	assert.Equal(t, true, field1.LoadContents)
	assert.Equal(t, 1, len(field1.Format))
	assert.Equal(t, "format.txt", field1.Format[0])
}

func TestParseCommandOutputRecordSchema(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_record_schema.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Outputs["recordSchema"].Type.Wrapped
	assert.IsType(t, CommandOutputRecordSchema{}, ty)
	record := ty.(CommandOutputRecordSchema)
	assert.Equal(t, "label", record.Label)
	assert.Equal(t, "doc1 doc2", record.Doc)
	assert.Equal(t, "typeName", record.Name)
	assert.Equal(t, 2, len(record.Fields))

	field0 := record.Fields["field0"]
	assert.IsType(t, StringType{}, field0.Type.Wrapped)
	assert.Equal(t, "field0 doc", field0.Doc)
	assert.Equal(t, "field0Label", field0.Label)
	assert.Equal(t, false, field0.Streamable)

	field1 := record.Fields["field1"]
	assert.IsType(t, FileType{}, field1.Type.Wrapped)
	assert.Equal(t, true, field1.Streamable)
	assert.Equal(t, 1, len(field1.Format))
	assert.Equal(t, "format.txt", field1.Format[0])
}

func TestParseCommandOutputRecordSchemaFieldsMap(t *testing.T) {
	tool, err := ParseFile("test/test_command_output_record_schema_map.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	ty := tool.Outputs["recordSchema"].Type.Wrapped
	assert.IsType(t, CommandOutputRecordSchema{}, ty)
	record := ty.(CommandOutputRecordSchema)
	assert.Equal(t, "label", record.Label)
	assert.Equal(t, "doc1 doc2", record.Doc)
	assert.Equal(t, "typeName", record.Name)
	assert.Equal(t, 2, len(record.Fields))

	field0 := record.Fields["field0"]
	assert.IsType(t, StringType{}, field0.Type.Wrapped)
	assert.Equal(t, "field0 doc", field0.Doc)
	assert.Equal(t, "field0Label", field0.Label)
	assert.Equal(t, false, field0.Streamable)

	field1 := record.Fields["field1"]
	assert.IsType(t, FileType{}, field1.Type.Wrapped)
	assert.Equal(t, true, field1.Streamable)
	assert.Equal(t, 1, len(field1.Format))
	assert.Equal(t, "format.txt", field1.Format[0])
}
