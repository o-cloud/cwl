package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseRequirementMap(t *testing.T) {
	tool, err := ParseFile("test/test_requirement_map.yaml", t)
	if err != nil {
		t.Fatalf("%s", err)
	}
	assert.Equal(t, 1, len(tool.Requirements))
	requirement := tool.Requirements[0]
	assert.IsType(t, DockerRequirement{}, requirement)
	r := requirement.(DockerRequirement)
	assert.Equal(t, "geodata/gdal", r.DockerPull)
	assert.Equal(t, "http://localhost:5000", r.DockerLoad)
	assert.Equal(t, "stringValue", r.DockerFile)
	assert.Equal(t, "importValue", r.DockerImport)
	assert.Equal(t, "gdal", r.DockerImageId)
	assert.Equal(t, "outputDir", r.DockerOutputDirectory)
	assert.Equal(t, "DockerRequirement", r.Type)
}
