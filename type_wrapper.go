package cwl

import (
	"encoding/json"
	"fmt"
)

type CommandInputTypeWrapper struct {
	Wrapped CommandInputType
}

func (c *CommandInputTypeWrapper) UnmarshalJSON(bytes []byte) error {
	if len(bytes) == 0 {
		return nil
	}
	var m map[string]interface{}
	if err := json.Unmarshal(bytes, &m); err != nil {
		return err
	}
	t, ok := m["type"]
	if !ok {
		return fmt.Errorf("cannot parse commandInputType, there is no field type in %+v", m)
	}
	str, ok := t.(string)
	if !ok {
		return fmt.Errorf("cannot parse commandInputType, expected field type to be a string not a %T", t)
	}
	scalarType, err := unmarshalScalarType(bytes, str)
	if err != nil {
		return err
	}
	if scalarType != nil {
		c.Wrapped = scalarType
		return nil
	}
	switch str {
	case "record":
		var o CommandInputRecordSchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	case "enum":
		var o CommandInputEnumSchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	case "union":
		var o CommandInputUnionSchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	case "array":
		var o CommandInputArraySchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	default:
		return fmt.Errorf("cannot parse commandInputType, unknown type %s", str)
	}
	return nil
}

func unmarshalScalarType(bytes []byte, typeStr string) (CwlScalarType, error) {
	switch typeStr {
	case "boolean":
		var o BooleanType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "int":
		var o IntegerType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "long":
		var o LongType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "float":
		var o FloatType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "double":
		var o DoubleType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "string":
		var o StringType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "file":
		var o FileType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "directory":
		var o DirectoryType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "dateRange":
		var o DateRangeType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	case "boundingBox":
		var o BoundingBoxType
		if err := json.Unmarshal(bytes, &o); err != nil {
			return nil, err
		}
		return o, nil
	}
	return nil, nil
}

func (c CommandInputTypeWrapper) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.Wrapped)
}

type CommandOutputTypeWrapper struct {
	Wrapped CommandOutputType
}

func (c CommandOutputTypeWrapper) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.Wrapped)
}

func (c *CommandOutputTypeWrapper) UnmarshalJSON(bytes []byte) error {
	if len(bytes) == 0 {
		return nil
	}
	var m map[string]interface{}
	if err := json.Unmarshal(bytes, &m); err != nil {
		return err
	}
	t, ok := m["type"]
	if !ok {
		return fmt.Errorf("cannot parse commandOutputType, there is no field type in %+v", m)
	}
	str, ok := t.(string)
	if !ok {
		return fmt.Errorf("cannot parse commandOutputType, expected field type to be a string not a %T", t)
	}
	scalarType, err := unmarshalScalarType(bytes, str)
	if err != nil {
		return err
	}
	if scalarType != nil {
		c.Wrapped = scalarType
		return nil
	}
	switch str {
	case "record":
		var o CommandOutputRecordSchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	case "enum":
		var o CommandOutputEnumSchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	case "union":
		var o CommandOutputUnionSchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	case "array":
		var o CommandOutputArraySchema
		if err := json.Unmarshal(bytes, &o); err != nil {
			return err
		}
		c.Wrapped = o
	default:
		return fmt.Errorf("cannot parse commandOutputType, unknown type %s", str)
	}
	return nil
}
