package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CommandOutputBinding struct {
	LoadContents bool     `json:"loadContents"`
	Glob         []string `json:"glob"`
	OutputEval   string   `json:"outputEval"`
}

func parseCommandOutputBinding(node yaml.Node) (CommandOutputBinding, error) {
	switch node.(type) {
	case yaml.Map:
		binding := new(CommandOutputBinding)
		for key, value := range node.(yaml.Map) {
			switch key {
			case "loadContents":
				b, err := value.Boolean()
				if err != nil {
					return *binding, fmt.Errorf("cannot parse command output binding, cannot parse loadContents, %s", err)
				}
				binding.LoadContents = b
			case "glob":
				list, err := value.StringList()
				if err != nil {
					return *binding, fmt.Errorf("cannot parse command output binding, cannot parse glob, %s", err)
				}
				binding.Glob = list
			case "outputEval":
				eval, err := value.String()
				if err != nil {
					return *binding, fmt.Errorf("cannot parse command output binding, cannot parse outputEval, %s", err)
				}
				binding.OutputEval = eval
			}
		}
		return *binding, nil
	default:
		return CommandOutputBinding{}, fmt.Errorf("cannot parse command output binding, expected type Map found %T", node)
	}
}
