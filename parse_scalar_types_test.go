package cwl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseScalarType(t *testing.T) {
	tool, err := ParseFile("test/test_scalar_types.cwl", t)
	if err != nil {
		t.Fatalf("%s", err)
	}

	p, ok := tool.Inputs["booleanInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, BooleanType{}, p.Type.Wrapped)
	assert.Equal(t, "boolean", p.Type.Wrapped.(BooleanType).Type)

	p, ok = tool.Inputs["intInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, IntegerType{}, p.Type.Wrapped)
	assert.Equal(t, "integer", p.Type.Wrapped.(IntegerType).Type)

	p, ok = tool.Inputs["longInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, LongType{}, p.Type.Wrapped)
	assert.Equal(t, "long", p.Type.Wrapped.(LongType).Type)

	p, ok = tool.Inputs["floatInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, FloatType{}, p.Type.Wrapped)
	assert.Equal(t, "float", p.Type.Wrapped.(FloatType).Type)

	p, ok = tool.Inputs["doubleInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, DoubleType{}, p.Type.Wrapped)
	assert.Equal(t, "double", p.Type.Wrapped.(DoubleType).Type)

	p, ok = tool.Inputs["stringInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, StringType{}, p.Type.Wrapped)
	assert.Equal(t, "string", p.Type.Wrapped.(StringType).Type)

	p, ok = tool.Inputs["fileInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, FileType{}, p.Type.Wrapped)
	assert.Equal(t, "file", p.Type.Wrapped.(FileType).Type)

	p, ok = tool.Inputs["directoryInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, DirectoryType{}, p.Type.Wrapped)
	assert.Equal(t, "directory", p.Type.Wrapped.(DirectoryType).Type)

	p, ok = tool.Inputs["dateRangeInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, DateRangeType{}, p.Type.Wrapped)
	assert.Equal(t, "dateRange", p.Type.Wrapped.(DateRangeType).Type)

	p, ok = tool.Inputs["boundingBoxInput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, BoundingBoxType{}, p.Type.Wrapped)
	assert.Equal(t, "boundingBox", p.Type.Wrapped.(BoundingBoxType).Type)

	output, ok := tool.Outputs["booleanOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, BooleanType{}, output.Type.Wrapped)
	assert.Equal(t, "boolean", output.Type.Wrapped.(BooleanType).Type)

	output, ok = tool.Outputs["intOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, IntegerType{}, output.Type.Wrapped)
	assert.Equal(t, "integer", output.Type.Wrapped.(IntegerType).Type)

	output, ok = tool.Outputs["longOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, LongType{}, output.Type.Wrapped)
	assert.Equal(t, "long", output.Type.Wrapped.(LongType).Type)

	output, ok = tool.Outputs["floatOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, FloatType{}, output.Type.Wrapped)
	assert.Equal(t, "float", output.Type.Wrapped.(FloatType).Type)

	output, ok = tool.Outputs["doubleOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, DoubleType{}, output.Type.Wrapped)
	assert.Equal(t, "double", output.Type.Wrapped.(DoubleType).Type)

	output, ok = tool.Outputs["stringOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, StringType{}, output.Type.Wrapped)
	assert.Equal(t, "string", output.Type.Wrapped.(StringType).Type)

	output, ok = tool.Outputs["fileOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, FileType{}, output.Type.Wrapped)
	assert.Equal(t, "file", output.Type.Wrapped.(FileType).Type)

	output, ok = tool.Outputs["directoryOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, DirectoryType{}, output.Type.Wrapped)
	assert.Equal(t, "directory", output.Type.Wrapped.(DirectoryType).Type)

	output, ok = tool.Outputs["dateRangeOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, DateRangeType{}, output.Type.Wrapped)
	assert.Equal(t, "dateRange", output.Type.Wrapped.(DateRangeType).Type)

	output, ok = tool.Outputs["boundingBoxOutput"]
	assert.Equal(t, true, ok)
	assert.IsType(t, BoundingBoxType{}, output.Type.Wrapped)
	assert.Equal(t, "boundingBox", output.Type.Wrapped.(BoundingBoxType).Type)
}
