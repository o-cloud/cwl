package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CommandInputRecordSchema struct {
	InputBinding CommandLineBinding                 `json:"inputBinding"`
	Fields       map[string]CommandInputRecordField `json:"fields"`
	commonRecordSchema
}

func (c CommandInputRecordSchema) isCommandInputType() {}

type CommandInputRecordField struct {
	Type         CommandInputTypeWrapper `json:"type"`
	LoadContents bool                    `json:"loadContents"`
	InputBinding CommandLineBinding      `json:"inputBinding"`
	commonRecordField
}

type CommandOutputRecordSchema struct {
	Fields map[string]CommandOutputRecordField `json:"fields"`
	commonRecordSchema
}

type CommandOutputRecordField struct {
	Type          CommandOutputTypeWrapper `json:"type"`
	OutputBinding CommandOutputBinding     `json:"outputBinding"`
	commonRecordField
}

type commonRecordSchema struct {
	Label string `json:"label"`
	Doc   string `json:"doc"`
	Name  string `json:"name"`
	Type  string `json:"type"`
}

type commonRecordField struct {
	Doc        string   `json:"doc"`
	Label      string   `json:"label"`
	Streamable bool     `json:"streamable"`
	Format     []string `json:"format"`
}

func (c CommandOutputRecordSchema) isCommandOutputType() {}

func parseCommonRecordSchema(node yaml.Map) (commonRecordSchema, error) {
	common := new(commonRecordSchema)
	common.Type = "record"
	for k, v := range node {
		switch k {
		case "label":
			s, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse record type, cannot parse label, %s", err)
			}
			common.Label = s
		case "doc":
			doc, err := parseDoc(v)
			if err != nil {
				return *common, fmt.Errorf("cannot parse record type, %s", err)
			}
			common.Doc = doc
		case "name":
			s, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse record type, cannot parse name, %s", err)
			}
			common.Name = s
		}
	}
	return *common, nil
}

func parseCommandOutputRecordSchema(node yaml.Node) (CommandOutputRecordSchema, error) {
	switch node.(type) {
	case yaml.Map:
		record := new(CommandOutputRecordSchema)
		common, err := parseCommonRecordSchema(node.(yaml.Map))
		if err != nil {
			return *record, err
		}
		record.Label = common.Label
		record.Name = common.Name
		record.Doc = common.Doc
		record.Type = common.Type
		for k, v := range node.(yaml.Map) {
			switch k {
			case "fields":
				fields, err := parseOutputRecordFields(v)
				if err != nil {
					return *record, fmt.Errorf("cannot parse record type, %s", err)
				}
				record.Fields = fields
			}
		}
		return *record, nil
	default:
		return CommandOutputRecordSchema{}, fmt.Errorf("cannot parse record output type, expected Map, found %T", node)
	}
}

func parseCommandInputRecordSchema(node yaml.Node) (CommandInputRecordSchema, error) {
	switch node.(type) {
	case yaml.Map:
		record := new(CommandInputRecordSchema)
		common, err := parseCommonRecordSchema(node.(yaml.Map))
		if err != nil {
			return *record, err
		}
		record.Label = common.Label
		record.Name = common.Name
		record.Doc = common.Doc
		record.Type = common.Type
		for k, v := range node.(yaml.Map) {
			switch k {
			case "inputBinding":
				binding, err := parseCommandLineBinding(v)
				if err != nil {
					return *record, fmt.Errorf("cannot parse record input type, %s", err)
				}
				record.InputBinding = *binding
			case "fields":
				fields, err := parseInputRecordFields(v)
				if err != nil {
					return *record, fmt.Errorf("cannot parse record input type, %s", err)
				}
				record.Fields = fields
			}
		}
		return *record, nil
	default:
		return CommandInputRecordSchema{}, fmt.Errorf("cannot parse record input type, expected map found %T", node)
	}
}

func parseOutputRecordFields(node yaml.Node) (map[string]CommandOutputRecordField, error) {
	switch node.(type) {
	case yaml.List:
		fields := make(map[string]CommandOutputRecordField, len(node.(yaml.List)))
		for i, v := range node.(yaml.List) {
			field, ok := v.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], expected map found %T", i, v)
			}
			nameField, ok := field["name"]
			if !ok {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], cannot find name", i)
			}
			name, err := nameField.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], cannot parse name, %s", i, err)
			}
			recordField, err := parseOutputRecordField(field)
			if err != nil {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], %s", i, err)
			}
			fields[name] = recordField
		}
		return fields, nil
	case yaml.Map:
		fields := make(map[string]CommandOutputRecordField, len(node.(yaml.Map)))
		for key, value := range node.(yaml.Map) {
			field, ok := value.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse fields, cannot parse field %s, expected map found %T", key, value)
			}
			recordField, err := parseOutputRecordField(field)
			if err != nil {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields %s, %s", key, err)
			}
			fields[key] = recordField
		}
		return fields, nil
	default:
		return nil, fmt.Errorf("cannot parse fields, expected type list or map, found %T", node)
	}
}

func parseInputRecordFields(node yaml.Node) (map[string]CommandInputRecordField, error) {
	switch node.(type) {
	case yaml.List:
		fields := make(map[string]CommandInputRecordField, len(node.(yaml.List)))
		for i, v := range node.(yaml.List) {
			field, ok := v.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], expected map found %T", i, v)
			}
			nameField, ok := field["name"]
			if !ok {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], cannot find name", i)
			}
			name, err := nameField.String()
			if err != nil {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], cannot parse name, %s", i, err)
			}
			recordField, err := parseInputRecordField(field)
			if err != nil {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields[%d], %s", i, err)
			}
			fields[name] = recordField
		}
		return fields, nil
	case yaml.Map:
		fields := make(map[string]CommandInputRecordField, len(node.(yaml.Map)))
		for key, value := range node.(yaml.Map) {
			field, ok := value.(yaml.Map)
			if !ok {
				return nil, fmt.Errorf("cannot parse fields, cannot parse field %s, expected map found %T", key, value)
			}
			recordField, err := parseInputRecordField(field)
			if err != nil {
				return nil, fmt.Errorf("cannot parse fields, cannot parse fields %s, %s", key, err)
			}
			fields[key] = recordField
		}
		return fields, nil
	default:
		return nil, fmt.Errorf("cannot parse fields, expected type list or map, found %T", node)
	}
}

func parseCommonRecordField(node yaml.Map) (commonRecordField, error) {
	common := new(commonRecordField)
	for k, v := range node {
		switch k {
		case "label":
			str, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse label, %s", err)
			}
			common.Label = str
		case "doc":
			str, err := parseDoc(v)
			if err != nil {
				return *common, err
			}
			common.Doc = str
		case "streamable":
			b, err := v.Boolean()
			if err != nil {
				return *common, fmt.Errorf("cannot parse streamable, %s", err)
			}
			common.Streamable = b
		case "format":
			format, err := v.StringList()
			if err != nil {
				return *common, fmt.Errorf("cannot parse format, %s", err)
			}
			common.Format = format
		}
	}
	return *common, nil
}

func parseOutputRecordField(node yaml.Map) (CommandOutputRecordField, error) {
	field := new(CommandOutputRecordField)
	typeNode, ok := node["type"]
	if !ok {
		return *field, fmt.Errorf("cannot find type")
	}
	t, err := parseOutputType(typeNode)
	if err != nil {
		return *field, err
	}
	field.Type = CommandOutputTypeWrapper{Wrapped: t}
	common, err := parseCommonRecordField(node)
	if err != nil {
		return *field, err
	}
	field.Doc = common.Doc
	field.Label = common.Label
	field.Format = common.Format
	field.Streamable = common.Streamable
	for k, v := range node {
		switch k {
		case "outputBinding":
			binding, err := parseCommandOutputBinding(v)
			if err != nil {
				return *field, err
			}
			field.OutputBinding = binding
		}
	}
	return *field, err
}

func parseInputRecordField(node yaml.Map) (CommandInputRecordField, error) {
	field := new(CommandInputRecordField)
	typeNode, ok := node["type"]
	if !ok {
		return *field, fmt.Errorf("cannot find type")
	}
	t, err := parseInputType(typeNode)
	if err != nil {
		return *field, err
	}
	field.Type = CommandInputTypeWrapper{Wrapped: t}
	common, err := parseCommonRecordField(node)
	if err != nil {
		return *field, err
	}
	field.Doc = common.Doc
	field.Label = common.Label
	field.Format = common.Format
	field.Streamable = common.Streamable
	for k, v := range node {
		switch k {
		case "inputBinding":
			binding, err := parseCommandLineBinding(v)
			if err != nil {
				return *field, err
			}
			field.InputBinding = *binding
		case "loadContents":
			b, err := v.Boolean()
			if err != nil {
				return *field, fmt.Errorf("cannot parse loadContents, %s", err)
			}
			field.LoadContents = b
		}
	}
	return *field, nil
}
