package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CwlScalarType interface {
	isCommandInputType()
	isCommandOutputType()
}

type BooleanType struct {
	Type string `json:"type"`
}

type IntegerType struct {
	Type string `json:"type"`
}

type LongType struct {
	Type string `json:"type"`
}

type FloatType struct {
	Type string `json:"type"`
}

type DoubleType struct {
	Type string `json:"type"`
}

type StringType struct {
	Type string `json:"type"`
}

type FileType struct {
	Type string `json:"type"`
}

type DirectoryType struct {
	Type string `json:"type"`
}

type DateRangeType struct {
	Type string `json:"type"`
}

type BoundingBoxType struct {
	Type string `json:"type"`
}

func parseCwlType(node yaml.Node) (CwlScalarType, error) {
	switch node.(type) {
	case yaml.Scalar:
		str, err := node.String()
		if err != nil {
			return nil, fmt.Errorf("cannot parse cwl type, %s", node)
		}
		switch str {
		case "boolean":
			return BooleanType{Type: "boolean"}, nil
		case "int":
			return IntegerType{Type: "integer"}, nil
		case "long":
			return LongType{Type: "long"}, nil
		case "float":
			return FloatType{Type: "float"}, nil
		case "double":
			return DoubleType{Type: "double"}, nil
		case "string":
			return StringType{Type: "string"}, nil
		case "File":
			return FileType{Type: "file"}, nil
		case "Directory":
			return DirectoryType{Type: "directory"}, nil
		case "DateRange":
			return DateRangeType{Type: "dateRange"}, nil
		case "BoundingBox":
			return BoundingBoxType{Type: "boundingBox"}, nil
		default:
			return nil, fmt.Errorf("cannot parse cwl type, unknown type %s", str)
		}
	default:
		return nil, fmt.Errorf("cannot parse cwl type, expected string found %T", node)
	}
}

func (b BooleanType) isCommandInputType()      {}
func (b BooleanType) isCommandOutputType()     {}
func (b IntegerType) isCommandInputType()      {}
func (b IntegerType) isCommandOutputType()     {}
func (b LongType) isCommandInputType()         {}
func (b LongType) isCommandOutputType()        {}
func (b FloatType) isCommandInputType()        {}
func (b FloatType) isCommandOutputType()       {}
func (b DoubleType) isCommandInputType()       {}
func (b DoubleType) isCommandOutputType()      {}
func (b StringType) isCommandInputType()       {}
func (b StringType) isCommandOutputType()      {}
func (b FileType) isCommandInputType()         {}
func (b FileType) isCommandOutputType()        {}
func (b DirectoryType) isCommandInputType()    {}
func (b DirectoryType) isCommandOutputType()   {}
func (b DateRangeType) isCommandInputType()    {}
func (b DateRangeType) isCommandOutputType()   {}
func (b BoundingBoxType) isCommandInputType()  {}
func (b BoundingBoxType) isCommandOutputType() {}
