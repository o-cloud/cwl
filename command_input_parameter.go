package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type Inputs map[string]CommandInputParameter

type CommandInputParameter struct {
	Label        string                  `json:"label"`
	Doc          string                  `json:"doc"`
	Streamable   bool                    `json:"streamable"`
	LoadContents bool                    `json:"loadContents"`
	InputBinding CommandLineBinding      `json:"inputBinding"`
	Format       []string                `json:"format"`
	Type         CommandInputTypeWrapper `json:"type"`
	InputType    string                  `json:"inputType"`
}

type CommandInputType interface {
	isCommandInputType()
}

func parseInputs(node yaml.Node) (*Inputs, error) {
	switch node.(type) {
	case yaml.List:
		result := make(Inputs, len(node.(yaml.List)))
		for i, input := range node.(yaml.List) {
			id, err := extractId(input)
			if err != nil {
				return nil, fmt.Errorf("cannot parse input[%d], %s", i, err)
			}
			cip := new(CommandInputParameter)
			err = parseInput(input, cip)
			if err != nil {
				return nil, fmt.Errorf("cannot parse input %s, %s", id, err)
			}
			result[id] = *cip
		}
		return &result, nil
	case yaml.Map:
		result := make(Inputs, len(node.(yaml.Map)))
		for key, value := range node.(yaml.Map) {
			cip := new(CommandInputParameter)
			err := parseInput(value, cip)
			if err != nil {
				return nil, fmt.Errorf("cannot parse input %s, %s", key, err)
			}
			result[key] = *cip
		}
		return &result, nil
	default:
		return nil, fmt.Errorf("cannot parse inputs, expected array or map, found %T", node)
	}
}

func parseInput(node yaml.Node, input *CommandInputParameter) error {
	switch node.(type) {
	case yaml.Map:
		inputType, ok := node.(yaml.Map)["inputType"]
		if !ok {
			return fmt.Errorf("input must have a key inputType")
		}
		inputTypeStr, err := inputType.String()
		if err != nil {
			return fmt.Errorf("cannont parse inputType, %s", err)
		}
		if inputTypeStr != "orchestrator" && inputTypeStr != "pipeline" && inputTypeStr != "execution" && inputTypeStr != "definition" {
			return fmt.Errorf("inputType value must be one of orchestrator, pipeline, execution or definition, found %s", inputTypeStr)
		}
		input.InputType = inputTypeStr
		for k, v := range node.(yaml.Map) {
			switch k {
			case "label":
				str, err := v.String()
				if err != nil {
					return fmt.Errorf("cannot parse label, %s", err)
				}
				input.Label = str
			case "doc":
				doc, err := parseDoc(v)
				if err != nil {
					return err
				}
				input.Doc = doc
			case "streamable":
				b, err := v.Boolean()
				if err != nil {
					return fmt.Errorf("cannot parse streamable, %s", err)
				}
				input.Streamable = b
			case "loadContents":
				b, err := v.Boolean()
				if err != nil {
					return fmt.Errorf("cannot parse loadContent, %s", err)
				}
				input.LoadContents = b
			case "inputBinding":
				binding, err := parseCommandLineBinding(v)
				if err != nil {
					return err
				}
				input.InputBinding = *binding
			case "format":
				formats, err := v.StringList()
				if err != nil {
					return fmt.Errorf("cannot parse format, %s", err)
				}
				input.Format = formats
			case "type":
				inputType, err := parseInputType(v)
				if err != nil {
					return err
				}
				input.Type = CommandInputTypeWrapper{Wrapped: inputType}
			}
		}
	default:
		return fmt.Errorf("expected map found %T", node)
	}
	return nil
}

func extractId(node yaml.Node) (string, error) {
	switch node.(type) {
	case yaml.Map:
		id, ok := node.(yaml.Map)["id"]
		if !ok {
			return "", fmt.Errorf("cannot parse id, key id doesn't exists in %v", node)
		}
		str, err := id.String()
		if err != nil {
			return "", fmt.Errorf("cannot parse id, %s", err)
		}
		return str, nil
	default:
		return "", fmt.Errorf("expected map, found %T", node)
	}
}

func parseInputType(node yaml.Node) (CommandInputType, error) {
	switch node.(type) {
	case yaml.Scalar:
		return parseCwlType(node)
	case yaml.Map:
		typeScalar, ok := node.(yaml.Map)["type"]
		if !ok {
			return nil, fmt.Errorf("cannot parse type, map doesn't contains a type key")
		}
		typeStr, err := typeScalar.String()
		if !ok {
			return nil, fmt.Errorf("cannot parse type, error while reading type key, %s", err)
		}
		switch typeStr {
		case "enum":
			return parseCommandInputEnumSchema(node)
		case "array":
			return parseCommandInputArraySchema(node)
		case "record":
			return parseCommandInputRecordSchema(node)
		default:
			return nil, fmt.Errorf("cannot parse type, unknown type %s", typeStr)
		}
	case yaml.List:
		return parseCommandInputUnionSchema(node)
	default:
		return nil, fmt.Errorf("cannot parse type, expected string, map or list, found %T", node)
	}
}
