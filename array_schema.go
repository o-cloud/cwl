package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CommandInputArraySchema struct {
	Items        CommandInputTypeWrapper `json:"items"`
	InputBinding CommandLineBinding      `json:"inputBinding"`
	commonArraySchema
}

type CommandOutputArraySchema struct {
	Items CommandOutputTypeWrapper `json:"items"`
	commonArraySchema
}

type commonArraySchema struct {
	Label string `json:"label"`
	Doc   string `json:"doc"`
	Name  string `json:"name"`
	Type  string `json:"type"`
}

func (c CommandInputArraySchema) isCommandInputType() {}

func (c CommandOutputArraySchema) isCommandOutputType() {}

func parseCommonArraySchema(node yaml.Map) (commonArraySchema, error) {
	common := new(commonArraySchema)
	common.Type = "array"
	for k, v := range node {
		switch k {
		case "label":
			s, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse array type, cannot parse label, %s", err)
			}
			common.Label = s
		case "doc":
			doc, err := parseDoc(v)
			if err != nil {
				return *common, fmt.Errorf("cannot parse array type, %s", err)
			}
			common.Doc = doc
		case "name":
			s, err := v.String()
			if err != nil {
				return *common, fmt.Errorf("cannot parse array type, cannot parse name, %s", err)
			}
			common.Name = s
		}
	}
	return *common, nil
}

func parseCommandOutputArraySchema(node yaml.Node) (CommandOutputArraySchema, error) {
	switch node.(type) {
	case yaml.Map:
		array := new(CommandOutputArraySchema)
		common, err := parseCommonArraySchema(node.(yaml.Map))
		if err != nil {
			return *array, err
		}
		array.Doc = common.Doc
		array.Label = common.Label
		array.Name = common.Name
		array.Type = common.Type
		items, ok := node.(yaml.Map)["items"]
		if !ok {
			return *array, fmt.Errorf("cannot parse array output type, key items doesn't exists")
		}
		itemsType, err := parseOutputType(items)
		if err != nil {
			return *array, fmt.Errorf("cannot parse array output type, cannot parse items, %s", err)
		}
		array.Items = CommandOutputTypeWrapper{Wrapped: itemsType}
		return *array, nil
	default:
		return CommandOutputArraySchema{}, fmt.Errorf("cannot parse array output type, expected map found %T", node)
	}
}

func parseCommandInputArraySchema(node yaml.Node) (CommandInputArraySchema, error) {
	switch node.(type) {
	case yaml.Map:
		array := new(CommandInputArraySchema)
		common, err := parseCommonArraySchema(node.(yaml.Map))
		if err != nil {
			return *array, nil
		}
		array.Doc = common.Doc
		array.Label = common.Label
		array.Name = common.Name
		array.Type = common.Type
		items, ok := node.(yaml.Map)["items"]
		if !ok {
			return *array, fmt.Errorf("cannot parse array input type, key items doesn't exists")
		}
		itemsType, err := parseInputType(items)
		if err != nil {
			return *array, fmt.Errorf("cannot parse array input type, cannot parse items, %s", err)
		}
		array.Items = CommandInputTypeWrapper{Wrapped: itemsType}
		for k, v := range node.(yaml.Map) {
			switch k {
			case "inputBinding":
				binding, err := parseCommandLineBinding(v)
				if err != nil {
					return *array, fmt.Errorf("cannot parse array input type, %s", err)
				}
				array.InputBinding = *binding
			}
		}
		return *array, nil
	default:
		return CommandInputArraySchema{}, fmt.Errorf("cannot parse array input type, expected map found %T", node)
	}
}
