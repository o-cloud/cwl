package cwl

import (
	"fmt"
	"gitlab.com/o-cloud/cwl/yaml"
)

type CommandLineBinding struct {
	IsFilled      bool   `json:"isFilled"`
	Position      int    `json:"position"`
	Prefix        string `json:"prefix"`
	Separate      bool   `json:"separate"`
	ItemSeparator string `json:"itemSeparator"`
	ValueFrom     string `json:"valueFrom"`
	ShellQuote    bool   `json:"shellQuote"`
}

func parseCommandLineBinding(node yaml.Node) (*CommandLineBinding, error) {
	switch node.(type) {
	case yaml.Map:
		m := node.(yaml.Map)
		binding := new(CommandLineBinding)
		//set default values
		binding.Position = 0
		binding.Separate = true
		binding.ShellQuote = true
		binding.IsFilled = true
		for key, v := range m {
			switch key {
			case "position":
				number, err := v.Integer()
				if err != nil {
					return nil, fmt.Errorf("cannot parse binding %s, %s", key, err)
				}
				binding.Position = number
			case "prefix":
				str, err := v.String()
				if err != nil {
					return nil, fmt.Errorf("cannot parse binding %s, %s", key, err)
				}
				binding.Prefix = str
			case "separate":
				b, err := v.Boolean()
				if err != nil {
					return nil, fmt.Errorf("cannot parse binding %s, %s", key, err)
				}
				binding.Separate = b
			case "itemSeparator":
				str, err := v.String()
				if err != nil {
					return nil, fmt.Errorf("cannot parse binding %s, %s", key, err)
				}
				binding.ItemSeparator = str
			case "valueFrom":
				str, err := v.String()
				if err != nil {
					return nil, fmt.Errorf("cannot parse binding %s, %s", key, err)
				}
				binding.ValueFrom = str
			case "shellQuote":
				b, err := v.Boolean()
				if err != nil {
					return nil, fmt.Errorf("cannot parse binding %s, %s", key, err)
				}
				binding.ShellQuote = b
			}
		}
		return binding, nil
	default:
		return nil, fmt.Errorf("cannot parse binding, expected Map, found %T", node)
	}
}
